name := "Kafka-Training"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.apache.kafka" %% "kafka" % "0.9.0.1",
  "org.json4s" %% "json4s-native" % "3.4.0"
)


mainClass in(Compile, run) := Some("com.dotography.example.App")

traceLevel in run := 0
