package com.dotography.example.kafka

import java.util.Properties
import java.util.concurrent.Executors

import kafka.consumer.{ConsumerConfig, KafkaStream, Consumer => KConsumer}
import kafka.message.MessageAndMetadata
import kafka.serializer.{DefaultDecoder, StringDecoder}

import scala.util.Try


object Consumer {

  private val effectiveConfig = new Properties() {
    put("group.id", s"kafka-training-${java.util.UUID.randomUUID.toString}")
    put("zookeeper.connect", "192.168.3.7:2181") // TODO externalize to config
  }

  private val consumerConnector = KConsumer.create(new ConsumerConfig(effectiveConfig))

  private val executor = Executors.newFixedThreadPool(1)

  def cousumer(topic: String, f: String => Unit): Unit = {

    val topicCountMap = Map(topic -> 1)
    val valueDecoder = new StringDecoder
    val keyDecoder = valueDecoder
    val consumerMap = consumerConnector.createMessageStreams(topicCountMap, keyDecoder, valueDecoder)

    val consumerThreads = consumerMap.get(topic) match {
      case Some(streams) => streams.view.zipWithIndex map {
        case (stream, threadId) =>
          new ConsumerTask(
            stream,
            ConsumerTaskContext(threadId, effectiveConfig),
            (a: MessageAndMetadata[_, _], b: ConsumerTaskContext, c: Option[_]) => Try(a.message.asInstanceOf[String]).foreach(f),
            (c: ConsumerTaskContext) => None,
            (c: ConsumerTaskContext, t: Option[_]) => ())
      }
      case _ => Seq()
    }
    consumerThreads foreach executor.submit

  }

  def shutdown() {
    println(Console.GREEN + "Shutting down Kafka consumer connector" + Console.RESET)
    consumerConnector.shutdown()
    println(Console.GREEN + "Shutting down thread pool of consumer tasks" + Console.RESET)
    executor.shutdown()
  }
}

class ConsumerTask[K, V, T, C <: ConsumerTaskContext](stream: KafkaStream[K, V],
                                                      context: C,
                                                      f: (MessageAndMetadata[K, V], C, Option[T]) => Unit,
                                                      startup: (C) => Option[T],
                                                      shutdown: (C, Option[T]) => Unit)
  extends Runnable {

  private var t: Option[T] = _

  @volatile private var shutdownAlreadyRanOnce = false

  override def run() {
    //    println(s"Consumer task of thread ${context.threadId} entered run()")
    println(s"${Console.GREEN}Connect to kafka.${Console.RESET}")
    t = startup(context)
    try {
      stream foreach {
        case msg: MessageAndMetadata[_, _] =>
          //          println(s"Thread ${context.threadId} received message: " + msg)
          f(msg, context, t)
        case _ => println(s"Received unexpected message type from broker")
      }
      gracefulShutdown()
    }
    catch {
      case e: InterruptedException => println(s"Consumer task of thread ${context.threadId} was interrupted")
    }
  }

  def gracefulShutdown() {
    if (!shutdownAlreadyRanOnce) {
      //      println("Performing graceful shutdown")
      shutdownAlreadyRanOnce = true
      shutdown(context, t)
    }
    else println("Graceful shutdown requested but it already ran once, so it will not be run again.")
  }

  Runtime.getRuntime.addShutdownHook(new Thread() {
    override def run() {
      println("Shutdown hook triggered!")
      gracefulShutdown()
    }
  })

}

case class ConsumerTaskContext(threadId: Int, config: Properties)
