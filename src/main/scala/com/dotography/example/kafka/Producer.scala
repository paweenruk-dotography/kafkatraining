package com.dotography.example.kafka

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}

object Producer {
  private def producer: KafkaProducer[String, String] = new KafkaProducer[String, String](new Properties() {
    put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.3.7:9092") // TODO externalize to config
    put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.ByteArraySerializer")
    put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
  })

  def sendToKafka(message: String): Unit = {
    producer.send(new ProducerRecord[String, String]("kafka_training", message))
  }

  def sendToKafka(messages: List[String]): Unit = {
    messages.foreach(sendToKafka)
  }
}
