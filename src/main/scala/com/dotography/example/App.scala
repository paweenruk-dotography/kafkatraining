package com.dotography.example


object App {
  def main(args: Array[String]) {

    print("Name: ")
    val name = scala.io.StdIn.readLine()
    kafka.Producer.sendToKafka(Message(name, "Join!!").toJson)

    val consumer = kafka.Consumer.cousumer("kafka_training", s => {
      Message.fromString(s).foreach {
        case m@Message(n, _) if n == name => ()
        case Message(n, m) => println(s"${Console.MAGENTA}$n: $m${Console.RESET}")
      }
    })

    for (message <- io.Source.stdin.getLines) {
      if (message == ":q") {
        kafka.Consumer.shutdown()
        sys.exit(0)
      }
      if (message.trim.length != 0)
        kafka.Producer.sendToKafka(Message(name, message).toJson)
    }

  }
}

case class Message(name: String, message: String) {

  import org.json4s.NoTypeHints
  import org.json4s.native.Serialization

  implicit val format = Serialization.formats(NoTypeHints)

  def toJson = {
    Serialization.write(this)
  }
}

object Message {

  import org.json4s.NoTypeHints
  import org.json4s.native.Serialization
  import org.json4s.native.JsonMethods._

  implicit val format = Serialization.formats(NoTypeHints)

  def fromString(str: String): Option[Message] = {
    parse(str).extractOpt[Message]
  }
}
